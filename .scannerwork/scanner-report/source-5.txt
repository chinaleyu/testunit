package example.mock;

import com.webridge.easymock.demo.ClassTested;
import com.webridge.easymock.demo.service.Collaborator;
import org.easymock.EasyMockRule;
import org.easymock.EasyMockSupport;
import org.easymock.Mock;
import org.easymock.TestSubject;
import org.junit.Rule;
import org.junit.Test;

/**
 * @author Charles
 * 1.Create the mock
 * 2.Have it set to the tested class
 * 3.Record what we expect the mock to do
 * 4.Tell all mocks we are now doing the actual testing
 * 5.Test
 * 6.Make sure everything that was supposed to be called was called
 */
public class EasyMockTest extends EasyMockSupport {

    @Rule
    public EasyMockRule rule = new EasyMockRule(this);

    @Mock
    private Collaborator collaborator; // 1

    @TestSubject
    private ClassTested classUnderTest = new ClassTested(); // 2

    @Test
    public void addDocument() {
        collaborator.documentAdded("New Document"); // 3
        replayAll(); // 4
        classUnderTest.addDocument("New Document", "content"); // 5
        verifyAll(); // 6
    }
}
