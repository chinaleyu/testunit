package suite;

import example.Demo;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 * @author Charles
 * @date 2019-12-18
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({Demo.class})
public class RootSuite {

}
