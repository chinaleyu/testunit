package example;

import com.webridge.easymock.service.EasyMockService;
import org.easymock.Mock;
import org.easymock.internal.MocksControl;
import org.junit.Assert;
import org.junit.Test;
import org.easymock.EasyMock;
/**
 * @author Charles
 */
public class EasyMockTest {


    @Test
    public void testMock(){

        EasyMockService mock = EasyMock.createMock(EasyMockService.class);
        //设定Mock对象的预期行为和输出
        EasyMock.expect(mock.addition(1,2)).andReturn(3);
        //将Mock对象切换到replay状态
        EasyMock.replay(mock);
        //进行单元测试
        Assert.assertEquals(3, mock.addition(1,2));
        //对Mock进行行为验证
        EasyMock.verify(mock);
    }



}
