package example.mock;

import com.webridge.easymock.demo.ClassTested;
import com.webridge.easymock.demo.service.Collaborator;
import org.easymock.*;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * @author Charles
 */
//@RunWith(EasyMockRunner.class)
public class MockAnnotationTest {

    @Mock(type = MockType.NICE, name = "mockName", fieldName = "someField")
    private Collaborator mockName;

    @Mock(type = MockType.STRICT, name = "anotherMock", fieldName = "someOtherField")
    private Collaborator anotherMock;

    @Rule
    public EasyMockRule mocks = new EasyMockRule(this);

    @Mock
    private Collaborator mock; // 1

    @TestSubject
    private ClassTested classUnderTest = new ClassTested(); // 2

    @Test
    public void testRemoveNonExistingDocument() {
        EasyMock.replay(mock);
        classUnderTest.removeDocument("Does not exist");
    }
}
