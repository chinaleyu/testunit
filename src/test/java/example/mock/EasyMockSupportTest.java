package example.mock;

import com.webridge.easymock.demo.ClassTested;
import com.webridge.easymock.demo.service.Collaborator;
import org.easymock.EasyMockRunner;
import org.easymock.EasyMockSupport;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * @author Charles
 */
@RunWith(EasyMockRunner.class)
public class EasyMockSupportTest extends EasyMockSupport {

    private Collaborator firstCollaborator;
    private Collaborator secondCollaborator;
    private ClassTested classUnderTest;

    @Before
    public void setup() {
        classUnderTest = new ClassTested();
    }

    @Test
    public void addDocument() {
        // creation phase
        firstCollaborator = mock(Collaborator.class);
        secondCollaborator = mock(Collaborator.class);
        classUnderTest.addListener(firstCollaborator);
        classUnderTest.addListener(secondCollaborator);
        // recording phase
        firstCollaborator.documentAdded("New Document");
        secondCollaborator.documentAdded("New Document");
        replayAll(); // replay all mocks at once

        // test
        classUnderTest.addDocument("New Document", new byte[0]);
        verifyAll(); // verify all mocks at once
    }
}
