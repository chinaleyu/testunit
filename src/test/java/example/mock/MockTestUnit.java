package example.mock;

import com.webridge.easymock.demo.ClassTested;
import com.webridge.easymock.demo.service.Collaborator;
import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;

/**
 * @author Charles
 */
public class MockTestUnit {

    private ClassTested classTested;

    private Collaborator mock;

    @Before
    public void setUp() {
        mock = EasyMock.createMock(Collaborator.class); // 1
        classTested = new ClassTested();
        classTested.setListener(mock);
    }

    @Test
    public void testRemoveNonExistingDocument() {
        // 2 (we do not expect anything)
        EasyMock.replay(mock);// 3
        classTested.removeDocument("Does not exist");
    }




}
