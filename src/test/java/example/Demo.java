package example;

import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.Collection;

@RunWith(SpringJUnit4ClassRunner.class)
public class Demo {

    private int param;

    private int result;

    public Demo(int param, int result) {
        this.param = param;
        this.result = result;
    }

    @Parameterized.Parameters
    public static Collection data() {
        return Arrays.asList(new Object[][] { { 1, 2 }, { 2, 3 }, { 4, 5 }, });
    }


    @Test
    public void demo(){

    }

    @Test(expected=NullPointerException.class)
    public void demo1(){

    }

    @Test(timeout=5000)
    public void demo2(){

    }

    @Test
    @Ignore("unfinished")
    public void demo3(){
        //do something....
    }

    @Before
    public void demo4(){

    }

    @After
    public void demo5(){

    }

    @BeforeClass
    public static void demo6(){
        System.out.println("BeforeClass");
    }

    @AfterClass
    public static void demo7(){
        System.out.println("AfterClass");
    }

}
