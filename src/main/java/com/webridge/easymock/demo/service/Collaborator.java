package com.webridge.easymock.demo.service;

/**
 * @author Charles
 */
public interface Collaborator {

    void documentAdded(String title);

    void removeDocument(String title);

    byte voteForRemovals(String title);

    void documentRemoved(String title);
}
