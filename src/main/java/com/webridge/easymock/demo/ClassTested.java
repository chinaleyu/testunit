package com.webridge.easymock.demo;

import com.webridge.easymock.demo.service.Collaborator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Charles
 */
public class ClassTested {

    private Collaborator listener;

    private List<Collaborator> listeners;

    public ClassTested(){
        listeners = new ArrayList<>();
    }

    public void addListener(Collaborator listener){
        listeners.add(listener);
    }

    public void setListener(Collaborator listener) {
        this.listener = listener;
    }

    public void addDocument(String title, String document) {
        listener.documentAdded(title);
    }

    public void addDocument(String title, byte[] bytes) {
        listener.documentAdded(title);
    }

    public void removeDocument(String title){
        listener.removeDocument(title);
    }

    public boolean removeDocuments(String title){
        listener.removeDocument(title);
        return true;
    }
}
